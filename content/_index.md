+++
title = "CloudForms and Ansible Deep Dive"
weight = 1
chapter = true
+++

# CloudForms and Ansible Deep Dive

This lab was delivered during Red Hat Tech Exchange 2019 in Vienna.

Short URL to this Lab: [http://bit.ly/cfrhtefy20](http://bit.ly/cfrhtefy20)
