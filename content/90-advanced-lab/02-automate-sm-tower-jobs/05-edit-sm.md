+++
title = "Edit the State Machine"
weight = 5
+++

### Edit the State Machine

To run the methods created in the last chapter of this lab during Virtual Machine Provisioning, we have to create new states into the State Machine.

- Click on the "VMProvision_VM" Class.

- On the right side of the screen click on the ***Schema*** tab.

- Click on ***Configuration*** -> ***Edit selected Schema***.

- Click on the little "+" symbol at the bottom of the list and enter the following details.

***Name*** vm_info

***Type*** State

***Data Type*** ab String

***Default Value:*** /Infrastructure/VM/Provisioning/StateMachines/Methods/vm_info

- Click on the little arrow pointing down to save this new row.

- Click on the little "+" symbol again and enter the following details.

***Name*** sat_register

***Type*** State

***Data Type*** ab String

***Default Value:*** /Infrastructure/VM/Provisioning/StateMachines/Methods/sat_register

- Click on the little arrow pointing down and ***Save*** to save the new Schema.

### Changing Class Sequence order

At this point, you introduced two new states into the states machine. As you might have noticed, they have been added at the end of the list and we need to move them to the proper place. Lets change the Class Sequence, by doing so the order of the States in the States Machine will change.

- Click on the "VMProvision_VM" Class.

- On the right side of the screen click on ***Schema*** tab.

- Click on ***Configuration*** -> ***Edit sequence***.

- Select "vm_info" and click on the pointing up arrow until the state "vm_info" is after the "AcquireMACAddress" state.

- Repeat the operation for "sat_register" state and move it after the "Register DHCP" state.

- Click ***Save***.

Now the states are in the correct order and you are ready to go and test the modified State Machine.
