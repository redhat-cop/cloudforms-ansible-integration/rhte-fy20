+++
title = "Test the State Machine"
weight = 6
+++

### Test the State Machine

Now is time to test all the work you´ve done during the lab.

- Navigate to ***Compute*** -> ***Infrastructure*** -> ***Virtual Machines***.

{{% notice note %}}
Make sure you're on the root "All VMs & Templates". If you see the details of a Virtual Machine or Template, the "Provision VMs" item will not appear in the "Lifecycle" menu.
{{% /notice %}}

- Click on ***Lifecycle*** -> ***Provision VMs***.

- In the Provision Virtual Machines screen select the "RHEL-7.6" Template and click ***Continue***.

- On the ***Requests*** tab the email address is the only required field. Enter your email address and optionally enter some data into the other fields as well.

- Select now the ***Catalog*** tab, in the bottom area enter a VM Name or if you want CloudForms to automatically name it for you just use "changeme".

- Click on ***Environment*** tab, select the "rhvh1.example.com" as a destination host and select the datastore "data".

- In the ***Network*** tab select the "ovirtmgmt" network from the dropdown list.

- Go to ***Customize*** tab and select "Specification" from the dropdown list and fill out the other details:

***Root Password:*** r3dh4t1!

***Address Mode:*** DHCP

***Customization Template***: oVirt cloud-init

- Click on ***Submit***.

- Wait until the request finishes and go to the Satellite UI (use the link from the RHPDS email), you will find your VM registered in the hosts area.

***Username:*** admin

***Password:*** r3dh4t1!

As you can see in this lab, the flexibility and possibilities that CloudForms offers to our customers, in combination with Embedded Ansible and Ansible Tower, have increased significantly in the 5.0 release.
