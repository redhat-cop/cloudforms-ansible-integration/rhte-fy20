+++
title = "Automate State Machines with Ansible Tower"
weight = 10
chapter = true
+++

# Automate State Machines with Ansible Tower

In this advanced lab you will practice how to run Ansible Tower Jobs from an Automate State Machine.
