+++
title = "Introduction to Tower Workflows"
weight = 2
+++

Ansible Tower allows users to create Workflows. Workflows are multiple Playbooks which run sequentially or in parallel and can be combined with a simple condition checker.

A typical use case is to define the provisioning process of a Virtual Machine as a Workflow. The Playbooks used to define the Workflow can be owned and maintained by different teams and they only have to define and implement clear interfaces between each team.

Workflow Example:

- run a Playbook to verify prerequisites are met

  - have a condition check which cancels the workflow if the condition is not met

- run a Playbook which creates the Virtual Machine

  - have a condition check which runs a rollback Playbook if creation failed

- run a Playbook which installs an application in the Virtual Machine

- run a Playbook which does basic Operating System configuration tasks

These last two Playbooks can run in parallel and don't have to wait for each other.
