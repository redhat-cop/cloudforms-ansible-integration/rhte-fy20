+++
title = "Custom Buttons with Embedded Ansible"
weight = 6
chapter = true
+++

# Custom Buttons with Embedded Ansible

In this chapter you will learn how to add custom buttons to the CloudForms UI and use Ansible to extend the out of the box functionality.
