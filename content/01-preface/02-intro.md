+++
title = "Introduction and Lab Access"
weight = 2
+++

This chapter will give you some context of the intention of writing these labs, on how to use them and how to provide feedback.

### How to access the lab environment

To deploy this lab, you need an account on the Red Hat Product Demo System. Log into RHPDS:

[https://rhpds.redhat.com](https://rhpds.redhat.com)

Navigate to **Services*** -> ***Catalogs*** and search for "CloudForms and Ansible Deep dive" in the section "Red Hat Tech Exchange 2019".

Click ***Order*** and fill out the order form. After a few minutes, you should receive an email with all the details you will need to access your lab environment.

***Username:*** admin

***Password:*** r3dh4t1!

Feel free to continue reading or jump to the next chapter and start with the [actual lab](../../02-configure-embedded/).

### How it all started

There are several places on the internet to find information about [CloudForms](http://www.redhat.com/cloudforms/) and [ManageIQ](http://www.manageiq.org). A good start is of course the [CloudForms Product Documentation](https://access.redhat.com/documentation/en-us/red_hat_cloudforms) or the [ManageIQ Project Documentation](http://manageiq.org/docs/). The [Red Hat CloudForms Blog](http://cloudformsblog.redhat.com) provides additional details and articles on how to use CloudForms. There is also a [ManageIQ Blog](http://manageiq.org/blog/) to follow news of the upstream project.

To learn everything there is to know about Automate, Peter McGowan's [Mastering CloudForms Automate](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/) and the [Addendum](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/) are great resources.

On top of that you can find articles on [Christian Jung's Blog](http://www.jung-christian.de) or the [TigerIQ Blog](http://tigeriq.co).

What is missing though is a consolidated source for Ansible related topics. Although the Ansible integration is improving with each release and constantly becomes easier to use, there are certain advanced topics which are not very well documented. It's also often useful to have all information in a consolidated sources.

This Lab is an attempt to provide such documentation. There is a series of [blog posts](https://cloudformsblog.redhat.com/2019/10/03/embedded-ansible-part-1-built-in-ansible-roles/) which give you the theoretical background to understand how Ansible and CloudForms work together.. This collection of labs will give you the hands on experience to use the theoretical knowledge in real world scenarios.

### How to run through this lab

Although it probably makes sense to run the lab in sequential order, each lab is supposed to be modular. This means, you can choose any of the top level chapters and run them in any particular order. For example, if you're only interested in how things work with embedded Ansible, you can skip the Tower related chapters entirely. That said, there are still some dependencies. You can not perform any of the Ansible Tower related labs, until you configured the Tower provider. There are indeed certain expectations towards the intelligence of the reader.

### How to build your own lab environment

Prerequisites:

- CloudForms Appliance with at least one Infrastructure or Cloud Provider

- Internet access from CloudForms to use git repositories

- Ansible Tower for the Tower related labs (or AWX?)
