+++
title = "Custom Buttons with Ansible Tower"
weight = 7
chapter = true
+++

# Custom Buttons with Ansible Tower

In this chapter you will learn how to add custom buttons to the CloudForms UI and use Ansible Tower Jobs to extend the out of the box functionality.
