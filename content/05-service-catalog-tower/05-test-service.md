+++
title = "Test Service Catalog Item"
weight = 5
+++

After all this work, we want to verify the Service Catalog Item works as expected.

### Verify Service Catalog Item

We want to test the Service Catalog Item works as expected.

- Navigate to ***Services*** -> ***Catalogs***.

{{% notice info %}}
If you followed the instructions by the letter, you should already be in this menu.
{{% /notice %}}

- Click on ***Service Catalogs*** in the accordion on the left.

- Click on ***InstallPackage*** in the ***Ansible Tower*** Service Catalog.

- Click on ***Order***.

***Service Name:*** Ansible Tower Test

***Limit:*** cfme008

***package_name:*** httpd

{{% notice info %}}
The package_name field might be read only. This will be changed in a separate part of this lab.
{{% /notice %}}

- Click ***Submit***.

You will be redirected to the "Requests" queue where you can monitor the progress of your order. As a result of the order a new "My Services" object was created. It contains some metadata and the Ansible Tower Job Output.

- Navigate to ***Services*** -> ***My Services***.

There should be only one object at this point in time - if there are multiple, try to find the latest one.

- Click on the Icon representing the Service.

- Click on the ***Job*** Tab to see the Ansible Playbook output.

Debug any problems you see, if there are any. Later parts of this lab will rely on a working Service Catalog Item and you should not proceed until all issues have been fixed.
